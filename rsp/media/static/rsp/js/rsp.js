$(document).ready(function() {
	/* tooltips */
	$('[rel=tooltip]').tooltip();
	/* popovers */
	$('[rel=popover]').popover();
	/* modals */
	$('[data-toggle="mymodal"]').bind('click',function(e) {
		e.preventDefault();
		$('#indicator').fadeIn();
		var url = $(this).attr('href');
		if (url.indexOf('#') == 0) {
			$('#response_modal').modal('open');
		} else {
			$.get(url, function(data) {
				$('#indicator').fadeOut();
				$('#response_modal').html(data);
				$('#response_modal').modal();
			}).success(function() {
				$('input:text:visible:first').focus();
			});
		}
	});
	$('#response_modal').on('hide', function () {
		$(".modal-body").empty();
	});
	/* date fields */
	$('.dateinput').datepicker({ format: "mm/dd/yyyy" });
	$('.dateinput').parent().parent().addClass('input-append');
	$('.dateinput').parent().append('<span class="add-on"><i class="icon-calendar"></span>');
	/* navigation */
	$('#navigation_autocomplete').yourlabsAutocomplete({
		url: '/navigation/',
		choiceSelector: 'a',
		placeholder: "Team/Leader name..."
	}).input.bind('selectChoice', function(e, choice, autocomplete) {
		document.location.href = choice.attr('href');
	});
	/* messages */
	var messageSelector = '#messages div';
	var messageContainer = '#messages';
	var closeSelector = '.message-close';
	var closeAllSelector = '.message-close-all';
	$.fn.messageClose = function() {
		$(this).fadeTo('fast', 0, function() {
			$(this).hide('fast', function() {
				$(this).remove();
			});
		});
	};
	$.fn.messageCloseTimeout = function(interval) {
		var _this = $(this);
		setTimeout(function() {
			_this.messageClose();
			var close = _this.find(closeSelector);
			if (close.attr('data-href') != '#') {
				$.ajax({
					url: $(this).attr('data-href')
				})
			}
		}, interval)
	};
	$(closeSelector).click(function(event) {
		event.preventDefault();
		if ($(this).attr('data-href') != '#') {
			$.ajax({
				url: $(this).attr('data-href')
			})
		}
		if ($(messageSelector).length <= 2) {
			$(closeAllSelector).messageClose();
		} else if ($(messageSelector).length <= 1) {
			$(messageContainer).messageClose();
		}
		$(this).closest(messageSelector).messageClose();
	});
	$(closeAllSelector).click(function(event) {
		event.preventDefault();
		$.ajax({
			url: $(this).attr('href')
		})
		$(this).messageClose();
		$(messageSelector).messageClose();
		$(messageContainer).messageClose();
	});
	$('[data-toggle="modal"]').bind('click',function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		if (url.indexOf('#') == 0) {
			$('#response_modal').modal('open');
		} else {
			$.get(url, function(data) {
				$('#response_modal').html(data);
				$('#response_modal').modal();
			}).success(function() { $('input:text:visible:first').focus(); });
		}
	});
});
