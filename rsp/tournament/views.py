import datetime
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.core import mail
from django.db.models import F, Q, Count
from django.forms.models import modelformset_factory
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.shortcuts import get_object_or_404, redirect
from tournament.forms import TournamentAddForm, ConfigAddForm, MapAddForm, TournamentEditForm
from tournament.forms import RoundEditForm, TeamAddForm, TeamEditForm, TeamTournamentEditForm
from tournament.forms import TournamentStaffEditForm, RulesAddForm, RulesEditForm, MatchReportAddForm
from tournament.forms import MatchReportConfirmationForm, MatchReportContestForm, MatchCastAddForm
from tournament.forms import MatchDemoAddForm
from tournament.models import Config, Tournament, Player, Match, MatchResult, MatchCast, MatchChange
from tournament.models import Round, Map, Team, TeamTournament, Rules, TournamentException, MatchDemo
from tournament.tasks import postprocess_round
from steamauth.utils import HttpResponseReload, staff_member_required
from steamauth.tasks import update_presence


def tournament_list(request):
	request.breadcrumbs("Tournaments", request.path_info if request.user.is_staff else "")
	return TemplateResponse(request, 'tournament/tournament_list.html', {
		'tournaments': Tournament.objects.all(),
	})


def tournament_details_results(request, slug, round_number):
	around = get_object_or_404(Round,
		tournament__slug=slug, number=round_number)
	request.breadcrumbs("Tournaments", reverse('tournament_list') if request.user.is_staff else "")
	request.breadcrumbs(around.tournament, around.tournament.get_absolute_url())
	request.breadcrumbs("Results", reverse('tournament_details',
		kwargs={'slug': slug, 'page': 'results'}))
	request.breadcrumbs(around, request.path_info)
	return TemplateResponse(request, 'tournament/tournament_details_results.html', {
		'tournament': around.tournament,
		'rounds': around.tournament.rounds.order_by('number'),
		'round': around,
		'results': list(MatchResult.objects.filter(match__round=around).select_related('match', 'tt', 'tt__team', 'opponent').filter(tt=F('match__home_tt')).order_by('-match__status')),
	})


def tournament_details_fixtures(request, slug):
	tournament = get_object_or_404(Tournament, slug=slug)
	around = tournament.current_round
	request.breadcrumbs("Tournaments", reverse('tournament_list') if request.user.is_staff else "")
	request.breadcrumbs(tournament, tournament.get_absolute_url())
	request.breadcrumbs("Fixtures", reverse('tournament_details',
		kwargs={'slug': tournament.slug, 'page': 'fixtures'}))
	request.breadcrumbs(around, request.path_info)
	return TemplateResponse(request, 'tournament/tournament_details_fixtures.html', {
		'tournament': tournament,
		'round': around,
		'matches': around.matches.prefetch_related('tts').filter(status=Match.STATUS_SCHEDULED),
	})


def tournament_details(request, slug, page='teams'):
	tournament = get_object_or_404(Tournament, slug=slug)
	request.breadcrumbs("Tournaments", reverse('tournament_list') if request.user.is_staff else "")
	request.breadcrumbs(tournament, tournament.get_absolute_url())
	template_name = 'tournament/tournament_details.html'
	if page in ('teams', 'schedule', 'results', 'rules', 'staff', 'resources'):
		request.breadcrumbs(page.title(), request.path_info)
		template_name = 'tournament/tournament_details_{0}.html'.format(page)
	context = {
		'tournament': tournament,
		'rounds': tournament.rounds.order_by('number'),
	}
	if page == 'teams':
		context['tts'] = tournament.tts.select_related('team').filter(is_suspended=False).order_by(
			'rank',
			'-ladder_points', '-ladder_points_won', '-match_points',
			'forfeits', '-match_points_won', 'matches_played', '-tiebreaker')
	elif page == 'schedule':
		context['rounds'] = tournament.rounds.select_related('played_map').order_by('number')
	elif page == 'resources':
		context['maps'] = Map.objects.filter(
			id__in=tournament.rounds.values('played_map').distinct()
		)
	elif page == 'staff':
		context['staff'] = tournament.staff.order_by('-level', 'username')
		if not cache.get('staff_{0}'.format(tournament.pk)):
			cache.set('staff_{0}'.format(tournament.pk), 600)
			update_presence(context['staff'].values_list('steamid', flat=True))
	elif page == 'rules':
		context['rules'] = tournament.rules
	return TemplateResponse(request, template_name, context)


@staff_member_required
def tournament_add(request):
	if not request.user.is_hero:
		messages.error(request, "You don't have permission to create tournaments.")
		return HttpResponseReload(request)
	if request.method == 'POST':
		form = TournamentAddForm(request.POST)
		if form.is_valid():
			tournament = form.save()
			messages.success(request, "Successfully created a new tournament.")
			return redirect(reverse('tournament_edit_rounds',
				kwargs={'slug': tournament.slug}))
	else:
		form = TournamentAddForm()
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs("Add new", request.path_info)
	return TemplateResponse(request, 'tournament/tournament_add.html', {
		'form': form,
	})


@staff_member_required
def tournament_edit(request, slug):
	tournament = get_object_or_404(Tournament, slug=slug)
	if not request.user.is_hero and not tournament.staff.filter(pk=request.user.id).exists():
		messages.error(request, "You don't have permission to edit properties of this tournament.")
		return HttpResponseReload(request)
	if request.method == 'POST':
		form = TournamentEditForm(request.POST, instance=tournament)
		if form.is_valid():
			tournament = form.save()
			messages.success(request, "Successfully modified tournament properties.")
			return redirect(reverse('tournament_details',
				kwargs={'slug': tournament.slug}))
	else:
		form = TournamentEditForm(instance=tournament)
	request.breadcrumbs(tournament, tournament.get_absolute_url())
	request.breadcrumbs("Edit rounds", request.path_info)
	return TemplateResponse(request, 'tournament/tournament_add.html', {
		'form': form,
	})


@staff_member_required
def tournament_edit_rounds(request, slug):
	tournament = get_object_or_404(Tournament, slug=slug)
	if not request.user.is_hero and not tournament.staff.filter(pk=request.user.id).exists():
		messages.error(request, "You don't have permission to edit rounds of this tournament.")
		return HttpResponseReload(request)
	if tournament.has_started():
		messages.error(request, "Modyfing round properties after tournament has begun is not allowed.")
		return HttpResponseReload(request)
	RoundsFormSet = modelformset_factory(Round, fields=('number', 'played_map'),
		extra=0, form=RoundEditForm)
	if request.method == 'POST':
		form_set = RoundsFormSet(request.POST, queryset=tournament.rounds.order_by('number'))
		if form_set.is_valid():
			rounds = form_set.save()
			messages.success(request, "Successfully changed round properties.")
			return redirect(tournament.get_absolute_url())
	else:
		form_set = RoundsFormSet(queryset=tournament.rounds.order_by('number'))
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs(tournament, tournament.get_absolute_url())
	request.breadcrumbs("Edit rounds", request.path_info)

	return TemplateResponse(request, 'tournament/tournament_edit_rounds.html', {
		'form_set': form_set,
		'tournament': tournament,
	})


@staff_member_required
def tournament_edit_teams(request, slug):
	tournament = get_object_or_404(Tournament, slug=slug)
	if not request.user.is_hero and not tournament.staff.filter(pk=request.user.id).exists():
		messages.error(request, "You don't have permission to edit team list of this tournament.")
		return HttpResponseReload(request)
	if tournament.state not in (Tournament.STATE_OPEN, Tournament.STATE_CLOSED):
		messages.error(request, "This tournament team list cannot be modified "
			"at this point of the tournament.")
		return HttpResponseReload(request)
	if request.method == 'POST':
		form = TeamTournamentEditForm(request.POST, instance=tournament)
		if form.is_valid():
			#tournament = form.save()
			teams = form.cleaned_data['teams']
			# first, delete remove team-tournament link
			# for all teams removed from the list in form
			TeamTournament.objects.filter(
				tournament=tournament).exclude(team__in=teams).delete()
			# secondly, exclude teams that are already in the tournament
			new_teams = Team.objects.filter(id__in=[t.id for t in teams]).exclude(
				stats__tournament=tournament)
			for team in new_teams:
				tt = TeamTournament(tournament=tournament, team=team)
				tt.save()
			messages.success(request, "Successfully changed tournament team list.")
			return redirect(tournament.get_absolute_url())
	else:
		form = TeamTournamentEditForm(instance=tournament)
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs(tournament, tournament.get_absolute_url())
	request.breadcrumbs("Edit teams", request.path_info)

	return TemplateResponse(request, 'tournament/tournament_edit_teams.html', {
		'form': form,
		'tournament': tournament,
	})


@staff_member_required
def tournament_edit_staff(request, slug):
	if not request.user.is_hero:
		messages.error(request, "You don't have permission to edit staff list.")
		return HttpResponseReload(request)
	tournament = get_object_or_404(Tournament, slug=slug)
	if request.method == 'POST':
		form = TournamentStaffEditForm(request.POST, instance=tournament)
		if form.is_valid():
			tournament = form.save()
			messages.success(request, "Successfully changed tournament staff list.")
			return redirect(tournament.get_absolute_url())
	else:
		form = TournamentStaffEditForm(instance=tournament)
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs(tournament, tournament.get_absolute_url())
	request.breadcrumbs("Edit staff", request.path_info)

	return TemplateResponse(request, 'tournament/tournament_edit_staff.html', {
		'form': form,
		'tournament': tournament,
	})


@staff_member_required
def tournament_generate_pairs(request, slug):
	if not request.user.is_hero:
		messages.error(request, "You don't have permission to generate pairs.")
		return HttpResponseReload(request)
	tournament = get_object_or_404(Tournament, slug=slug)

	try:
		next_round = tournament.rounds.filter(is_scheduled=False).order_by('number')[0]
	except IndexError:
		messages.error(request, "There's no unscheduled rounds.")
		return HttpResponseReload(request)
	try:
		previous_round = tournament.rounds.get(number=next_round.number-1)
	except Round.DoesNotExist:
		generate = True
	else:
		generate = not previous_round.is_happening()

	if not generate:
		messages.error(request, "Cannot generate pairs at this very moment.")
		return HttpResponseReload(request)

	try:
		next_round.generate_pairs()
	except TournamentException as e:
		messages.error(request, "Couldn't generate pairs: {0}".format(e))
	else:
		messages.success(request, "Generated pairs for next round.")

	return HttpResponseReload(request)


@staff_member_required
def tournament_post_round(request, slug):
	if not request.user.is_hero:
		messages.error(request, "You don't have permission to post-process a round.")
		return HttpResponseReload(request)
	tournament = get_object_or_404(Tournament, slug=slug)

	try:
		next_round = tournament.rounds.filter(is_postprocessed=False).order_by('number')[0]
	except IndexError:
		messages.error(request, "There's no round to post-process.")
		return HttpResponseReload(request)
	try:
		previous_round = tournament.rounds.get(number=next_round.number-1)
	except Round.DoesNotExist:
		generate = True
	else:
		generate = not previous_round.is_happening()

	if next_round.is_happening():
		messages.error(request, "Cannot post-process a round at this very moment.")
		return HttpResponseReload(request)

	postprocess_round(next_round.id)
	messages.success(request, "Post-processed a round.")

	return HttpResponseReload(request)


@login_required
def ready_up(request, slug):
	tournament = get_object_or_404(Tournament, slug=slug)
	try:
		team = request.user.get_team()
	except Team.DoesNotExist:
		messages.error(request, "You are not a leader of any team.")
		return HttpResponseReload(request)
	try:
		tt = TeamTournament.objects.get(tournament=tournament, team=team)
	except TeamTournament.DoesNotExist:
		messages.error(request, "You are not a leader of any team participating in this tournament.")
		return HttpResponseReload(request)
	tt.is_ready = True
	tt.save()
	messages.success(request, "You've confirmed your presence in this tournament!")
	return HttpResponseReload(request)


@staff_member_required
def tournament_player_list(request, slug):
	tournament = get_object_or_404(Tournament, slug=slug)
	if not request.user.is_hero and not tournament.staff.filter(pk=request.user.id).exists():
		messages.error(request, "You don't have permission to view player list of this tournament.")
		return HttpResponseReload(request)
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs(tournament, tournament.get_absolute_url())
	request.breadcrumbs("Players", request.path_info)

	return TemplateResponse(request, 'tournament/tournament_player_list.html', {
		'tournament': tournament,
		'players': tournament.rounds.matches.players.distinct(),
	})


def match_details(request, match_id):
	match = get_object_or_404(Match, pk=match_id)
	if request.user.is_authenticated():
		user_is_team_leader = request.user.id in match.tts.values_list('team__leader', flat=True)
	else:
		user_is_team_leader = False
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	request.breadcrumbs("Tournaments", reverse('tournament_list') if request.user.is_staff else "")
	request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
	request.breadcrumbs(match.round, reverse('tournament_details_results',
		kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
	request.breadcrumbs("{0}{1}".format(result.tt.team,
		" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), request.path_info)

	return TemplateResponse(request, 'tournament/match_details.html', {
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
		'casts': match.casts.all(),
		'demos': match.demos.all(),
		'user_is_team_leader': user_is_team_leader,
	})


@login_required
def match_report(request, match_id):
	match = get_object_or_404(Match, pk=match_id)
	if not request.user.id in match.tts.values_list('team__leader', flat=True):
		messages.error(request, "You don't have permission to modify the status report of this match.")
		return HttpResponseReload(request)
	if match.report:
		messages.error(request, "Match report has already been submitted.")
		return redirect(match.get_absolute_url())
	if not match.can_report():
		messages.error(request, "This match cannot be reported anymore.")
		return redirect(match.get_absolute_url())
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	if request.method == 'POST':
		form = MatchReportAddForm(request.POST)
		if form.is_valid():
			report = form.save()
			tt = match.tts.filter(team__leader=request.user)[0]
			report.submitted_by = tt.team 
			report.save()
			match.report = report
			match.status = Match.STATUS_REPORTED
			match.save()
			if not report.default_win and form.cleaned_data.get('status_message'):
				for p in form.cleaned_data.get('status_message')['players']:
					player, c = Player.objects.get_or_create(steamid=p['steamid'])
					player.nickname = p['nickname']
					player.save()
					report.players.add(player)
			messages.success(request, "Successfully stored your match report."
				" You have to wait until your opponent accepts it.")
			try:
				opponent_leader = match.tts.exclude(
					team=report.submitted_by)[0].team.leader
			except IndexError:
				pass
			else:
				if opponent_leader.email and opponent_leader.wants_emails:
					connection = mail.get_connection()
					connection.open()
					email = mail.EmailMessage(
						u"Your opponent has submitted a match report",
						render_to_string('tournament/email/report_waiting_for_you_to_confirm.txt', {
							'user': opponent_leader,
							'match': match,
						}),
						to=[opponent_leader.email], connection=connection)
					email.send()
					connection.close()
			return redirect(match.get_absolute_url())
	else:
		form = MatchReportAddForm()
	request.breadcrumbs("Tournaments", reverse('tournament_list') if request.user.is_staff else "")
	request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
	request.breadcrumbs(match.round, reverse('tournament_details_results',
		kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
	request.breadcrumbs("{0}{1}".format(result.tt.team,
		" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), match.get_absolute_url())
	request.breadcrumbs("Report", request.path_info)

	return TemplateResponse(request, 'tournament/match_report.html', {
		'form': form,
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
	})


@login_required
def match_report_accept(request, match_id):
	match = get_object_or_404(Match, pk=match_id)
	if not match.report:
		messages.error(request, "There's no report to accept.")
		return HttpResponseReload(request)
	if not request.user.id in match.tts.exclude(
			team=match.report.submitted_by).values_list('team__leader', flat=True):
		messages.error(request, "You don't have permission to accept this report.")
		return HttpResponseReload(request)
	if not match.can_confirm():
		messages.error(request, "This report cannot be accepted anymore.")
		return redirect(match.get_absolute_url())
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	if request.method == 'POST':
		form = MatchReportConfirmationForm(request.POST)
		if form.is_valid() and 'confirmation' in request.POST:
			match.post_accept()
			messages.success(request, "Successfully accepted match report.")
			return redirect(match.get_absolute_url())
	else:
		form = MatchReportConfirmationForm()
	request.breadcrumbs("Tournaments", reverse('tournament_list') if request.user.is_staff else "")
	request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
	request.breadcrumbs(match.round, reverse('tournament_details_results',
		kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
	request.breadcrumbs("{0}{1}".format(result.tt.team,
		" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), match.get_absolute_url())
	request.breadcrumbs("Accept report", request.path_info)

	return TemplateResponse(request, 'tournament/match_report_accept.html', {
		'form': form,
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
	})


@login_required
def match_report_contest(request, match_id):
	match = get_object_or_404(Match, pk=match_id)
	if not match.report:
		messages.error(request, "There's no report to contest.")
		return HttpResponseReload(request)
	if not request.user.id in match.tts.exclude(
			team=match.report.submitted_by).values_list('team__leader', flat=True):
		messages.error(request, "You don't have permission to contest this report.")
		return HttpResponseReload(request)
	if not match.can_confirm():
		messages.error(request, "This report cannot be contested anymore.")
		return redirect(match.get_absolute_url())
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	if request.method == 'POST':
		form = MatchReportContestForm(request.POST)
		if form.is_valid() and 'confirmation' in request.POST:
			match.report.contestation_notes = form.cleaned_data.get('contestation_notes')
			match.report.contested_at = datetime.datetime.now()
			match.report.save()
			match.status = Match.STATUS_CONTESTED
			match.save()
			messages.success(request, "You've contested opponent's report. A staff member will check it soon.")
			return redirect(match.get_absolute_url())
	else:
		form = MatchReportContestForm()
	request.breadcrumbs("Tournaments", reverse('tournament_list') if request.user.is_staff else "")
	request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
	request.breadcrumbs(match.round, reverse('tournament_details_results',
		kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
	request.breadcrumbs("{0}{1}".format(result.tt.team,
		" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), match.get_absolute_url())
	request.breadcrumbs("Contest report", request.path_info)

	return TemplateResponse(request, 'tournament/match_report_contest.html', {
		'form': form,
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
	})


def match_cast_list(request):
	request.breadcrumbs("Casts", request.path_info)
	return TemplateResponse(request, 'tournament/match_cast_list.html', {
		'casts': MatchCast.objects.select_related('match', 'created_by').order_by('-id')
	})


def match_cast_details(request, cast_id):
	cast = get_object_or_404(MatchCast, pk=cast_id)
	match = cast.match
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	if request.is_ajax():
		template_name = 'tournament/ajax/match_cast_details.html'
	else:
		template_name = 'tournament/match_cast_details.html'
		request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
		request.breadcrumbs(match.round, reverse('tournament_details_results',
			kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
		request.breadcrumbs("{0}{1}".format(result.tt.team,
			" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), match.get_absolute_url())
		request.breadcrumbs("Casted by {0}".format(cast.created_by), request.path_info)
	return TemplateResponse(request, template_name, {
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
		'cast': cast,
	})


@login_required
def match_cast_add(request, match_id):
	match = get_object_or_404(Match, pk=match_id)
	if not request.user.is_caster:
		messages.error(request, "You don't have permission to add match casts.")
		return HttpResponseReload(request)
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	if request.method == 'POST':
		form = MatchCastAddForm(request.POST)
		if form.is_valid():
			cast = form.save(commit=False)
			cast.match = match
			cast.created_by = request.user
			cast.save()
			messages.success(request, "Successfully stored your cast.")
			return redirect(match.get_absolute_url())
	else:
		form = MatchCastAddForm()
	request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
	request.breadcrumbs(match.round, reverse('tournament_details_results',
		kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
	request.breadcrumbs("{0}{1}".format(result.tt.team,
		" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), match.get_absolute_url())

	request.breadcrumbs("Add cast", request.path_info)
	return TemplateResponse(request, 'tournament/match_cast_add.html', {
		'form': form,
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
	})


@login_required
def match_demo_add(request, match_id):
	match = get_object_or_404(Match, pk=match_id)
	if not request.user.id in match.tts.values_list('team__leader', flat=True):
		messages.error(request, "You don't have permission to add a demo to this match.")
		return HttpResponseReload(request)
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	if request.method == 'POST':
		demo = MatchDemo(match=match, created_by=request.user)
		form = MatchDemoAddForm(request.POST, request.FILES, instance=demo)
		if form.is_valid():
			form.save()
			messages.success(request, "Successfully stored your demo.")
			return redirect(match.get_absolute_url())
	else:
		form = MatchDemoAddForm()
	request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
	request.breadcrumbs(match.round, reverse('tournament_details_results',
		kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
	request.breadcrumbs("{0}{1}".format(result.tt.team,
		" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), match.get_absolute_url())

	request.breadcrumbs("Add STV demo", request.path_info)
	return TemplateResponse(request, 'tournament/match_demo_add.html', {
		'form': form,
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
	})


CHANGE_SCORE_MESSAGES = {
	'revoke': "Revoked a report by {reported_by}",
	'accept': "Accepted a report by {reported_by}",
	'modify': "Modified score. {home_team} {home_score} - {away_score} {away_team}",
	'forfeit_home': "Home team ({home_team}) received a default loss",
	'forfeit_away': "Away team ({away_team}) received a default loss",
	'forfeit_both': "Both teams received a default loss",
}


@staff_member_required
def match_change_score(request, match_id):
	match = get_object_or_404(Match, pk=match_id)
	if not request.user.is_hero:
		messages.error(request, "You don't have permission to edit this match.")
		return HttpResponseReload(request)
	if match.status == Match.STATUS_BYE:
		messages.error(request, "You may not modify a bye match.")
		return HttpResponseReload(request)
	result = match.results.select_related('tt', 'tt__team', 'opponent').get(tt=match.home_tt)
	home_result = result
	try:
		away_result = match.results.select_related('tt', 'tt__team', 'opponent').exclude(pk=result.id)[0]
	except IndexError:
		# bye
		away_result = None
	if request.method == 'POST':
		context = {
			'home_team': result.tt.team,
			'away_team': result.opponent,
		}
		if match.report:
			context['reported_by'] = match.report.submitted_by
		if 'forfeit_both' in request.POST:
			action = 'forfeit_both'
			match.status = Match.STATUS_DEFAULTLOSS
			match.results.update(
				is_defaultloss=True,
				score=0,
				opponents_score=4,
				score_diff=-4,
				points_diff=-1,
				is_win=False,
			)
			match.save()
			home_result.tt.denormalize()
			if away_result:
				away_result.tt.denormalize()
			messages.success(request, "Successfully forfeited both teams.")
		elif 'forfeit_home' in request.POST:
			action = 'forfeit_home'
			match.status = Match.STATUS_RESOLVED
			match.save()
			home_result.clear()
			home_result.is_defaultloss = True
			home_result.score = 0
			home_result.opponents_score = 4
			home_result.save()
			if away_result:
				away_result.clear()
				away_result.is_defaultwin = True
				away_result.score = 1
				away_result.opponents_score = 0
				away_result.save()
				away_result.tt.denormalize()

			home_result.tt.denormalize()
			messages.success(request, "Successfully forfeited home team.")
		elif 'forfeit_away' in request.POST:
			action = 'forfeit_away'
			match.status = Match.STATUS_RESOLVED
			match.save()
			home_result.clear()
			home_result.is_defaultwin = True
			home_result.score = 1
			home_result.opponents_score = 0
			home_result.save()
			if away_result:
				away_result.clear()
				away_result.is_defaultloss = True
				away_result.score = 0
				away_result.opponents_score = 4
				away_result.save()
				away_result.tt.denormalize()
			home_result.tt.denormalize()
			messages.success(request, "Successfully forfeited away team.")
		elif 'revoke' in request.POST:
			action = 'revoke'
			if not match.report:
				messages.error(request, "There's no report to revoke.")
				return HttpResponseReload(request)
			if match.is_accepted():
				messages.error(request, "You cannot revoke an already accepted report.")
				return HttpResponseReload(request)
			report = match.report
			report.match_set.clear()
			match.status = Match.STATUS_SCHEDULED
			match.report = None
			match.save()
			report.delete()
			messages.success(request, "Successfully revoked current report.")
		elif 'accept' in request.POST:
			action = 'accept'
			if not match.report:
				messages.error(request, "You cannot accept a report that hasn't been submitted.")
				return HttpResponseReload(request)
			if match.status in (Match.STATUS_ACCEPTED, Match.STATUS_RESOLVED):
				messages.error(request, "You cannot accept a report after a result has been set.")
				return HttpResponseReload(request)
			match.post_accept()
			match.status = Match.STATUS_RESOLVED
			match.save()
			messages.success(request, "Successfully accepted current report.")
		elif 'modify' in request.POST:
			action = 'modify'
			match.status = Match.STATUS_RESOLVED
			match.save()
			home_score = int(request.POST.getlist('home_score')[0])
			away_score = int(request.POST.getlist('away_score')[0])
			context['home_score'] = home_score
			context['away_score'] = away_score
			home_result.clear()
			home_result.score = home_score
			home_result.opponents_score = away_score
			home_result.save()
			if away_result:
				away_result.clear()
				away_result.score = away_score
				away_result.opponents_score = home_score
				away_result.save()
				away_result.tt.denormalize()
			home_result.tt.denormalize()

			if match.report and not match.players.exists():
				match.players.add(*list(match.report.players.all()))
			messages.success(request, "Successfully stored new match result.")
		else:
			messages.error(request, "Unknown action sent.")
			return HttpResponseReload(request)

		message = CHANGE_SCORE_MESSAGES[action].format(**context)
		change = MatchChange()
		change.match = match
		change.created_by = request.user
		change.action = message
		change.notes = request.POST.get('notes', '')
		change.save()

		if 'notify' in request.POST:
			connection = mail.get_connection()
			connection.open()
			for r in [home_result, away_result]:
				if not r:
					continue
				if r.tt.team.leader.email and r.tt.team.leader.wants_emails:
					email = mail.EmailMessage(
						"Staff member has modified your match properies",
						render_to_string('tournament/email/score_changed.txt', {
							'user': r.tt.team.leader,
							'opponent_team': r.opponent,
							'staff_member': request.user,
							'message': message,
							'notes': change.notes,
						}),
						to=[r.tt.team.leader.email], connection=connection)
					email.send()
			connection.close()

		return redirect(match.get_absolute_url())
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs(match.round.tournament, match.round.tournament.get_absolute_url())
	request.breadcrumbs(match.round, reverse('tournament_details_results',
		kwargs={'slug':match.round.tournament.slug, 'round_number':match.round.number}))
	request.breadcrumbs("{0}{1}".format(result.tt.team,
		" vs {0}".format(result.opponent) if match.status != Match.STATUS_BYE else ", bye"), match.get_absolute_url())
	request.breadcrumbs("Modify score", request.path_info)

	return TemplateResponse(request, 'tournament/match_change_score.html', {
		'tournament': match.round.tournament,
		'match': match,
		'result': result,
	})


@staff_member_required
def match_list(request, slug, round_number):
	round = get_object_or_404(Round, number=round_number, tournament__slug=slug)
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs(round.tournament, round.tournament.get_absolute_url())
	request.breadcrumbs("Matches of {0}".format(round), request.path_info)

	return TemplateResponse(request, 'tournament/match_list.html', {
		'tournament': round.tournament,
		'results': MatchResult.objects.filter(match__round=round, tt=F('match__home_tt')).select_related('tt',
			'opponent', 'match', 'match__round'),
	})


@staff_member_required
def match_list_contested(request, slug, round_number):
	round = get_object_or_404(Round, number=round_number, tournament__slug=slug)
	request.breadcrumbs("Tournaments", reverse('tournament_list'))
	request.breadcrumbs(round.tournament, round.tournament.get_absolute_url())
	request.breadcrumbs("Contested reports of {0}".format(round), request.path_info)

	return TemplateResponse(request, 'tournament/match_list_contested.html', {
		'tournament': round.tournament,
		'results': MatchResult.objects.filter(match__round=round, tt=F('match__home_tt'),
			match__status=Match.STATUS_CONTESTED).select_related('tt',
			'opponent', 'match', 'match__round'),
	})


@staff_member_required
def player_force_medal(request, steamid, state):
	if not request.user.is_hero:
		return HttpResponseReload(request)

	STATES = {
		'grant': True,
		'neutral': None,
		'deny': False,
	}

	if state not in STATES.keys():
		messages.error(request, "Incorrect medal state.")
		return HttpResponseReload(request)

	player = get_object_or_404(Player, steamid=steamid)
	player.force_medals = STATES[state]
	player.medal_changed_by = request.user
	player.medal_changed_at = datetime.datetime.now()
	player.save()

	return HttpResponseReload(request)


@staff_member_required
def medal_overrides(request):
	return TemplateResponse(request, 'tournament/medal_overrides.html', {
		'medals': Player.objects.filter(force_medals__isnull=False).order_by('-medal_changed_at'),
	})


@staff_member_required
def player_details(request, steamid):
	player = get_object_or_404(Player, steamid=steamid)

	teams = TeamTournament.objects.filter(
		matches__players__in=[player.id]).annotate(
		played_matches=Count('id')).order_by('tournament', '-played_matches')

	request.breadcrumbs("Players", "")
	request.breadcrumbs(u"{0} ({1})".format(player.nickname, player.steamid), request.path_info)

	return TemplateResponse(request, 'tournament/player_details.html', {
		'player': player,
		'teams': teams,
	})


@staff_member_required
def team_list(request):
	request.breadcrumbs("Teams", request.path_info)
	return TemplateResponse(request, 'tournament/team_list.html', {
		'teams': Team.objects.select_related('leader'),
	})


def team_details(request, team_id):
	team = get_object_or_404(Team.objects.select_related('leader'), pk=team_id)
	request.breadcrumbs("Teams", reverse('team_list') if request.user.is_staff else "")
	request.breadcrumbs(team, request.path_info)
	return TemplateResponse(request, 'tournament/team_details.html', {
		'team': team,
		'players': team.player_set.order_by('-matches_played'),
		'tts': team.stats.select_related('tournament').prefetch_related(
			'results', 'results__opponent', 'results__match', 'results__match__round'),
	})


@staff_member_required
def team_add(request):
	if request.method == 'POST':
		form = TeamAddForm(request.POST)
		if form.is_valid():
			team = form.save()
			messages.success(request, "Successfully created a new team.")
			return redirect(team.get_absolute_url())
	else:
		form = TeamAddForm()
	request.breadcrumbs("Teams", reverse('team_list'))
	request.breadcrumbs("Add new", request.path_info)
	return TemplateResponse(request, 'tournament/team_add.html', {
		'form': form,
	})


@staff_member_required
def team_edit(request, team_id):
	team = get_object_or_404(Team, pk=team_id)
	if request.method == 'POST':
		form = TeamEditForm(request.POST, instance=team)
		if form.is_valid():
			team = form.save()
			messages.success(request, "Successfully updated team properties.")
			return redirect(team.get_absolute_url())
	else:
		form = TeamEditForm(instance=team)
	request.breadcrumbs("Teams", reverse('team_list'))
	request.breadcrumbs(team, team.get_absolute_url())
	request.breadcrumbs("Editing team properties", request.path_info)
	form.helper.form_action = request.path_info
	return TemplateResponse(request, 'tournament/team_edit.html', {
		'form': form,
		'team': team,
	})


def config_list(request):
	request.breadcrumbs("Configs", request.path_info)
	return TemplateResponse(request, 'tournament/config_list.html', {
		'configs': Config.objects.all(),
	})


def config_details(request, slug):
	config = get_object_or_404(Config, slug=slug)
	request.breadcrumbs("Configs", reverse('config_list'))
	request.breadcrumbs(config, request.path_info)
	return TemplateResponse(request, 'tournament/config_details.html', {
		'config': config,
	})


@staff_member_required
def config_add(request):
	if request.method == 'POST':
		form = ConfigAddForm(request.POST, request.FILES)
		if form.is_valid():
			config = form.save()
			messages.success(request, "Successfully created a new config.")
			return redirect(config.get_absolute_url())
	else:
		form = ConfigAddForm()
	request.breadcrumbs("Configs", reverse('config_list'))
	request.breadcrumbs("Add new", request.path_info)
	return TemplateResponse(request, 'tournament/config_add.html', {
		'form': form,
	})


def map_list(request):
	request.breadcrumbs("Maps", request.path_info)
	return TemplateResponse(request, 'tournament/map_list.html', {
		'maps': Map.objects.all(),
	})


def map_details(request, slug):
	amap = get_object_or_404(Map, slug=slug)
	request.breadcrumbs("Maps", reverse('map_list'))
	request.breadcrumbs(amap, request.path_info)
	return TemplateResponse(request, 'tournament/map_details.html', {
		'map': amap,
	})


@staff_member_required
def map_add(request):
	if request.method == 'POST':
		form = MapAddForm(request.POST, request.FILES)
		if form.is_valid():
			amap = form.save()
			messages.success(request, "Successfully stored a new map.")
			return redirect(amap.get_absolute_url())
	else:
		form = MapAddForm()
	request.breadcrumbs("Maps", reverse('map_list'))
	request.breadcrumbs("Add new", request.path_info)
	return TemplateResponse(request, 'tournament/map_add.html', {
		'form': form,
	})


def rules_list(request):
	request.breadcrumbs("Rules", request.path_info)
	return TemplateResponse(request, 'tournament/rules_list.html', {
		'rules_list': Rules.objects.all(),
	})


def rules_details(request, slug):
	rules = get_object_or_404(Rules, slug=slug)
	request.breadcrumbs("Rules", reverse('rules_list'))
	request.breadcrumbs(rules, request.path_info)
	return TemplateResponse(request, 'tournament/rules_details.html', {
		'rules': rules,
	})


@staff_member_required
def rules_add(request):
	if not request.user.is_hero:
		messages.error(request, "You don't have permission to add new rules pages.")
		return HttpResponseReload(request)
	if request.method == 'POST':
		form = RulesAddForm(request.POST)
		if form.is_valid():
			rules = form.save()
			messages.success(request, "Successfully stored a new rules page.")
			return redirect(rules.get_absolute_url())
	else:
		form = RulesAddForm()
	request.breadcrumbs("Rules", reverse('rules_list'))
	request.breadcrumbs("Add new", request.path_info)
	return TemplateResponse(request, 'tournament/rules_add.html', {
		'form': form,
	})


@staff_member_required
def rules_edit(request, slug):
	if not request.user.is_hero:
		messages.error(request, "You don't have permission to edit rules pages.")
		return HttpResponseReload(request)
	rules = get_object_or_404(Rules, slug=slug)

	if request.method == 'POST':
		form = RulesEditForm(request.POST, instance=rules)
		if form.is_valid():
			rules = form.save()
			messages.success(request, "Successfully updated rules page.")
			return redirect(rules.get_absolute_url())
	else:
		form = RulesEditForm(instance=rules)
	request.breadcrumbs("Rules", reverse('rules_list'))
	request.breadcrumbs(rules, rules.get_absolute_url())
	request.breadcrumbs("Editing rules page", request.path_info)
	form.helper.form_action = request.path_info
	return TemplateResponse(request, 'tournament/rules_edit.html', {
		'form': form,
		'rules': rules,
	})


@staff_member_required
def player_list(request):
	return TemplateResponse(request, 'tournament/player_list.html', {
		'players': Player.objects.all(),
	})


@login_required
def navigation_autocomplete(request):
	q = request.GET.get('q', '')
	context = {'q': q}

	queries = {}
	queries['teams_user'] = Team.objects.filter(
		Q(name__icontains=q) |
		Q(leader__username__icontains=q)
	)[:3]
	if request.user.is_staff:
		queries['users'] = get_user_model().objects.filter(
			Q(username__icontains=q) |
			Q(steamid__icontains=q)
		).distinct()[:3]
		queries['players'] = Player.objects.filter(
			Q(nickname__icontains=q) |
			Q(steamid__icontains=q)
		)[:3]
		queries['tts_player'] = TeamTournament.objects.filter(
			matches__players__steamid__exact=q).annotate(
			played_matches=Count('id')).order_by('-tournament', '-played_matches')[:3]

	context.update(queries)

	template_name = 'tournament/autocomplete/admin_navigation.html' \
		if request.user.is_staff else 'tournament/autocomplete/navigation.html'

	return TemplateResponse(request, template_name, context)


@staff_member_required
def admin_home(request):
	tournaments = Tournament.objects.filter(ends_at__gte=datetime.date.today())
	if not request.user.is_hero:
		tournaments = request.user.tournaments.filter(pk__in=tournaments)

	return TemplateResponse(request, 'tournament/admin_home.html', {
		'tournaments': tournaments,
	})


def user_home(request):
	return TemplateResponse(request, 'tournament/home.html', {
		'featured_tournaments': Tournament.objects.filter(is_featured=True),
	})


def home(request):
	view_func = admin_home if request.user.is_staff else user_home
	return view_func(request)


def about(request):
	request.breadcrumbs("What is RSP", request.path_info)
	return TemplateResponse(request, 'tournament/about.html')


def staff(request):
	request.breadcrumbs("Who runs RSP", request.path_info)
	staff = get_user_model().objects.filter(
		level__gte=get_user_model().LEVEL_SIDEKICK).order_by('username')
	return TemplateResponse(request, 'tournament/staff.html', {'staff': staff})


def issues(request):
	request.breadcrumbs("Issues", request.path_info)
	return TemplateResponse(request, 'tournament/issues.html')


def privacy(request):
	request.breadcrumbs("Privacy Policy", request.path_info)
	return TemplateResponse(request, 'tournament/privacy.html')


def what_is_love(request):
	request.breadcrumbs("What is Love", request.path_info)
	return TemplateResponse(request, 'tournament/what-is-love.html')


def tl_dr(request):
	request.breadcrumbs("Too long; didn't read", request.path_info)
	return TemplateResponse(request, 'tournament/tl_dr.html')