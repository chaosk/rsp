import autocomplete_light
from tournament.models import Team


class TeamAutocomplete(autocomplete_light.AutocompleteModelTemplate):
	search_fields = ('name', 'leader__username')
	choice_template = 'tournament/autocomplete/team.html'
	autocomplete_js_attributes = {
		'placeholder': 'Team name/leader name...'
	}

autocomplete_light.register(Team, TeamAutocomplete)