import datetime
from django.core import mail
from django.contrib.auth import get_user_model
from django.db.models import F
from django.template.loader import render_to_string
from tournament.models import Round, Match
import persistent_messages
from celery.task import task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

@task
def postprocess_round(round_id):
	"""
	Processes a round after it finishes.

	Auto-forfeits unconfirmed matches.

	Suspends teams with 2 forfeits.

	Refreshes ranks.
	"""

	round = Round.objects.get(pk=round_id)
	if round.is_postprocessed:
		logger.error("Round {0} (id={1}) of Tournament {1} (id=)"
			" has already been postprocessed".format(
			round, round_id, round.tournament, round.tournament_id
		))
		return False

	tournament = round.tournament
	matches = round.matches.all()

	for match in matches:
		if match.status == Match.STATUS_SCHEDULED:
			# both teams get forfeits.
			match.status = Match.STATUS_DEFAULTLOSS
			match.tts.update(
				forfeits=F('forfeits')+1,
				ladder_points=F('ladder_points')-1,
				ladder_points_lost=F('ladder_points_lost')+1,
				match_points=F('match_points')-4,
				match_points_lost=F('match_points_lost')+4
			)
			match.results.update(
				is_defaultloss=True,
				score=0,
				opponents_score=4,
				score_diff=-4,
				points_diff=-1,
				is_win=False,
			)

	to_suspend = tournament.tts.filter(is_suspended=False,
		forfeits__gte=2)

	try:
		suspended_by = get_user_model().get(steamid=1)
	except get_user_model().DoesNotExist:
		suspended_by = get_user_model().get(pk=1)

	to_suspend.update(
		is_suspended=True,
		suspended_by=suspended_by,
		suspended_at=datetime.datetime.now(),
		suspended_for=u"AUTO Forfeiting too many matches.")


	connection = mail.get_connection()
	connection.open()

	for tt in to_suspend:
		persistent_messages.add_async_message(
			persistent_messages.ERROR,
			"Your team has been suspended from {0} tournament for forfeiting "
			"too many matches. You may appeal to one of the staff members "
			"responsible for this tournament.".format(tournament),
			user=tt.team.leader,
		)
		if tt.team.leader.email and tt.team.leader.wants_emails:
			email = mail.EmailMessage(
				u"Your team has been suspended from the tournament",
				render_to_string('tournament/email/team_suspended.txt', {
					'user': tt.team.leader,
					'tt': tt,
				}),
				to=[tt.team.leader.email], connection=connection)
			email.send()
	connection.close()

	round.is_postprocessed = True
	round.save()