import re
from datetime import datetime
from hashlib import md5
from django.core.cache import cache
from django.conf import settings
from embedly import Embedly
from embeds.models import SavedEmbed

USER_AGENT = 'Mozilla/5.0 (compatible; django-embedly/0.2; +http://github.com/BayCitizen/)'

STATUS_PATTERN_YE_OLDE = re.compile(r"""^hostname: (?P<server_name>.*?)
version : .*? secure
udp/ip  : (?P<local_ip>[\d.]+):(?P<port>\d+)  \(public ip: (?P<public_ip>[\d.]+)\)
account : .*? (.*?)
map     : (?P<map_name>\w+) at: 0 x, 0 y, 0 z
players : (?P<clients>\d+) \((?P<max_clients>\d+) max\)

# userid name                uniqueid            connected ping loss state
(?P<players>.*)""", re.DOTALL | re.UNICODE)

PLAYER_PATTERN = re.compile(r"#\s*(?P<userid>\d+) \"(?P<nickname>.*?)\"\s*(?P<steamid>[STEAM_:\d]+)\s*(?P<connected>[:\d]+)\s*(?P<ping>\d+)\s*(?P<loss>\d+) (?P<state>\w+)\n?", re.UNICODE)


def parse_status(raw_status):
	"""
	Parses game status. Please note everything is returned as string (even numbers).
	"""

	"""
	HOORAY WINDOWS!
	"""
	raw_status = raw_status.replace("\r\n", "\n")

	players = [m.groupdict() for m in PLAYER_PATTERN.finditer(raw_status)]

	if not players:
		raise ValueError("Input cannot be recognized as status command output.")

	#if len(players) != int(matches_dict['clients']):
	#	raise ValueError(
	#		"Number of player rows ({0}) does not match"
	#		" connected clients count ({1}).".format(len(players), matches_dict['clients']))

	matches_dict = {
		'players': players,
	}

	return matches_dict


def embedly(url, maxwidth, thumbnail=False):
	key = make_cache_key(url, maxwidth)
	if not thumbnail:
		cached_html = cache.get(key)

		if cached_html:
			return cached_html

	try:
		embed = SavedEmbed.objects.get(url=url, maxwidth=maxwidth)
		cache.set(key, embed.html)
		return embed.html if not thumbnail else embed.thumbnail_url
	except SavedEmbed.DoesNotExist:
		# call embedly API
		client = Embedly(key=settings.EMBEDLY_KEY, user_agent=USER_AGENT)
		if maxwidth:
			oembed = client.oembed(url, maxwidth=maxwidth)
		else:
			oembed = client.oembed(url)

		if oembed.error:
			err_code = (oembed.data['error_code']
						if 'error_code' in oembed.data else 'Unknown')
			LOG.warn('Error fetching %s (%s)', url, err_code)
			return url

		# save result to database
		row, created = SavedEmbed.objects.get_or_create(url=url, maxwidth=maxwidth,
			defaults={'type': oembed.type})

	if oembed.type == 'photo':
		html = '<img src="%s" ' % oembed.url
		if maxwidth:
			html += 'width="%s" />' % maxwidth
		else:
			html += 'width="%s" height="%s" />' % (oembed.width, oembed.height)
	else:
		html = oembed.html

	if html:
		row.html = html
		row.thumbnail_url = oembed.thumbnail_url
		row.last_updated = datetime.now()
		row.save()

	cache.set(key, html, 86400)

	return html	if not thumbnail else oembed.thumbnail_url


def make_cache_key(url, maxwidth=None):
	md5_hash = md5(url).hexdigest()
	return "embeds.%s.%s" % (maxwidth if maxwidth else 'default', md5_hash)
