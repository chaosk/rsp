from django import forms
from tournament.models import Config, Tournament, Map, Round
from tournament.models import Team, Rules, MatchReport, MatchCast, MatchDemo
from tournament.utils import parse_status
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
import autocomplete_light


class TournamentAddForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_action = 'tournament_add'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(TournamentAddForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Tournament
		fields = ('name', 'short_name', 'slug', 'starts_at', 'configs', 'rules',
			'state', 'is_featured', 'ladder_rounds', 'playoff_rounds')


class TournamentEditForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(TournamentEditForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Tournament
		fields = ('name', 'short_name', 'configs', 'rules', 'state', 'is_featured')


# https://code.djangoproject.com/ticket/9321
class TeamTournamentEditForm(autocomplete_light.FixedModelForm):
	
	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(TeamTournamentEditForm, self).__init__(*args, **kwargs)

	class Meta:
		widgets = autocomplete_light.get_widgets_dict(Tournament)
		model = Tournament
		fields = ('teams',)


# https://code.djangoproject.com/ticket/9321
class TournamentStaffEditForm(autocomplete_light.FixedModelForm):
	
	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(TournamentStaffEditForm, self).__init__(*args, **kwargs)

	class Meta:
		widgets = autocomplete_light.get_widgets_dict(Tournament)
		model = Tournament
		fields = ('staff',)


class TeamAddForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_action = 'team_add'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(TeamAddForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Team


class TeamEditForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(TeamEditForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Team


class MatchReportAddForm(forms.ModelForm):
	status_message = forms.CharField(
		label='Status message',
		widget=forms.Textarea(),
		help_text='Paste entire status command output here. Leave blank when reporting default win.',
		required=False,
	)

	default_win = forms.BooleanField(
		label='Default win',
		help_text="Did your opponent forfeit the match? <strong>Do not check it if they didn't.</strong>",
		required=False,
	)

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(MatchReportAddForm, self).__init__(*args, **kwargs)

	def clean_team_score(self):
		data = self.cleaned_data['team_score']
		try:
			our_data = int(data)
		except ValueError:
			raise forms.ValidationError("Value provided is not a number.")
		if our_data > 4:
			raise forms.ValidationError("You may not input a value greater than 4.")
		if our_data < 0:
			raise forms.ValidationError("You may not input a value lower than 0.")

		return data

	def clean_opponents_score(self):
		data = self.cleaned_data['opponents_score']
		try:
			our_data = int(data)
		except ValueError:
			raise forms.ValidationError("Value provided is not a number.")

		if our_data > 4:
			raise forms.ValidationError("You may not input a value greater than 4.")
		if our_data < 0:
			raise forms.ValidationError("You may not input a value lower than 0.")

		return data

	def clean_status_message(self):
		data = self.cleaned_data['status_message']
		if self.cleaned_data.get('default_win'):
			return data
		try:
			data = parse_status(data)
		except ValueError as e:
			raise forms.ValidationError(e)

		return data

	def clean(self):
		cleaned_data = super(MatchReportAddForm, self).clean()
		team_score = cleaned_data.get('team_score')
		opponents_score = cleaned_data.get('opponents_score')

		if team_score == None or opponents_score == None:
			raise forms.ValidationError("Invalid value(s) of score field(s).")

		opponents_score = int(opponents_score)
		team_score = int(team_score)
		if team_score != 0 and team_score == opponents_score:
			raise forms.ValidationError(
				"You may not provide an equal value to both score fields (draws are not allowed).")

		if team_score + opponents_score > 7:
			raise forms.ValidationError("Sum of both score fields may not be higher than 7.")

		return cleaned_data

	class Meta:
		model = MatchReport
		fields = ('team_score', 'opponents_score', 'default_win', 'notes')


class MatchReportConfirmationForm(forms.Form):
	pass


class MatchReportContestForm(forms.ModelForm):
	
	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('confirmation', 'Submit'))
		self.helper.add_input(Submit('cancel', 'Cancel', css_class='btn-inverse'))
		super(MatchReportContestForm, self).__init__(*args, **kwargs)


	class Meta:
		model = MatchReport
		fields = ('contestation_notes',)


class MatchCastAddForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(MatchCastAddForm, self).__init__(*args, **kwargs)

	class Meta:
		model = MatchCast
		fields = ('title', 'url')



class MatchDemoAddForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(MatchDemoAddForm, self).__init__(*args, **kwargs)

	def clean_demo_file(self):
		_file = self.cleaned_data.get('demo_file')
		if not _file:
			raise forms.ValidationError("No demo file provided")
		chunk = _file.chunks().next()
		if chunk[0:8] == "HL2DEMO\0" and chunk[276:536].strip("\0") == "SourceTV Demo":
			return _file
		raise forms.ValidationError("Invalid file provided")

	class Meta:
		model = MatchDemo
		fields = ('demo_file', 'start_tick', 'notes')


class ConfigAddForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_action = 'config_add'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(ConfigAddForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Config


class MapAddForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_action = 'map_add'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(MapAddForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Map


class RulesAddForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_action = 'rules_add'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(RulesAddForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Rules
		fields = ('name', 'slug', 'rules_raw')


class RulesEditForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(RulesEditForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Rules
		fields = ('name', 'slug', 'rules_raw')


class RoundEditForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(RoundEditForm, self).__init__(*args, **kwargs)

	class Meta:
		model = Round
		fields = ('number', 'played_map')
