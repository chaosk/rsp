# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tournament'
        db.create_table(u'tournament_tournament', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=70)),
            ('config', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Config'])),
            ('starts_at', self.gf('django.db.models.fields.DateField')()),
            ('ladder_rounds', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('playoff_rounds', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'tournament', ['Tournament'])

        # Adding model 'Config'
        db.create_table(u'tournament_config', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=70)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('config_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'tournament', ['Config'])

        # Adding model 'Round'
        db.create_table(u'tournament_round', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('tournament', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Tournament'])),
            ('starts_at', self.gf('django.db.models.fields.DateField')()),
            ('ends_at', self.gf('django.db.models.fields.DateField')()),
            ('is_scheduled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('round_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'tournament', ['Round'])

        # Adding model 'Match'
        db.create_table(u'tournament_match', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('round', self.gf('django.db.models.fields.related.ForeignKey')(related_name='matches', to=orm['tournament.Round'])),
            ('home_team', self.gf('django.db.models.fields.related.ForeignKey')(related_name='home_matches', to=orm['tournament.Team'])),
            ('winner', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='won_matches', null=True, to=orm['tournament.Team'])),
            ('status', self.gf('django.db.models.fields.CharField')(default='S', max_length=1)),
        ))
        db.send_create_signal(u'tournament', ['Match'])

        # Adding M2M table for field players on 'Match'
        db.create_table(u'tournament_match_players', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('match', models.ForeignKey(orm[u'tournament.match'], null=False)),
            ('player', models.ForeignKey(orm[u'tournament.player'], null=False))
        ))
        db.create_unique(u'tournament_match_players', ['match_id', 'player_id'])

        # Adding model 'MatchReport'
        db.create_table(u'tournament_matchreport', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('match', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reports', to=orm['tournament.Match'])),
            ('submitted_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('submitted_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Team'])),
            ('team_score', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('opponents_score', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('notes', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'tournament', ['MatchReport'])

        # Adding M2M table for field players on 'MatchReport'
        db.create_table(u'tournament_matchreport_players', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('matchreport', models.ForeignKey(orm[u'tournament.matchreport'], null=False)),
            ('player', models.ForeignKey(orm[u'tournament.player'], null=False))
        ))
        db.create_unique(u'tournament_matchreport_players', ['matchreport_id', 'player_id'])

        # Adding model 'MatchResult'
        db.create_table(u'tournament_matchresult', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('match', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Match'])),
            ('team', self.gf('django.db.models.fields.related.ForeignKey')(related_name='results', to=orm['tournament.Team'])),
            ('score', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('opponents_score', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('points_diff', self.gf('django.db.models.fields.SmallIntegerField')(null=True)),
            ('score_diff', self.gf('django.db.models.fields.SmallIntegerField')(null=True)),
            ('is_win', self.gf('django.db.models.fields.NullBooleanField')(default=None, null=True, blank=True)),
        ))
        db.send_create_signal(u'tournament', ['MatchResult'])

        # Adding model 'Team'
        db.create_table(u'tournament_team', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('tag', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('leader', self.gf('django.db.models.fields.related.ForeignKey')(related_name='team', to=orm['steamauth.SteamUser'])),
            ('has_server', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'tournament', ['Team'])

        # Adding model 'TeamTournament'
        db.create_table(u'tournament_teamtournament', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tournament', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Tournament'])),
            ('team', self.gf('django.db.models.fields.related.ForeignKey')(related_name='stats', to=orm['tournament.Team'])),
            ('forfeits', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('matches_played', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('ladder_points', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('ladder_points_won', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('ladder_points_lost', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('match_points', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('match_points_won', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('match_points_lost', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('is_alive', self.gf('django.db.models.fields.NullBooleanField')(default=None, null=True, blank=True)),
        ))
        db.send_create_signal(u'tournament', ['TeamTournament'])

        # Adding M2M table for field opponents on 'TeamTournament'
        db.create_table(u'tournament_teamtournament_opponents', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('teamtournament', models.ForeignKey(orm[u'tournament.teamtournament'], null=False)),
            ('team', models.ForeignKey(orm[u'tournament.team'], null=False))
        ))
        db.create_unique(u'tournament_teamtournament_opponents', ['teamtournament_id', 'team_id'])

        # Adding model 'Player'
        db.create_table(u'tournament_player', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('steamid', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'tournament', ['Player'])


    def backwards(self, orm):
        # Deleting model 'Tournament'
        db.delete_table(u'tournament_tournament')

        # Deleting model 'Config'
        db.delete_table(u'tournament_config')

        # Deleting model 'Round'
        db.delete_table(u'tournament_round')

        # Deleting model 'Match'
        db.delete_table(u'tournament_match')

        # Removing M2M table for field players on 'Match'
        db.delete_table('tournament_match_players')

        # Deleting model 'MatchReport'
        db.delete_table(u'tournament_matchreport')

        # Removing M2M table for field players on 'MatchReport'
        db.delete_table('tournament_matchreport_players')

        # Deleting model 'MatchResult'
        db.delete_table(u'tournament_matchresult')

        # Deleting model 'Team'
        db.delete_table(u'tournament_team')

        # Deleting model 'TeamTournament'
        db.delete_table(u'tournament_teamtournament')

        # Removing M2M table for field opponents on 'TeamTournament'
        db.delete_table('tournament_teamtournament_opponents')

        # Deleting model 'Player'
        db.delete_table(u'tournament_player')


    models = {
        u'steamauth.steamuser': {
            'Meta': {'object_name': 'SteamUser'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'steamid': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'tournament.config': {
            'Meta': {'object_name': 'Config'},
            'config_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'})
        },
        u'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'home_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'home_matches'", 'to': u"orm['tournament.Team']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'matches'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['tournament.Player']"}),
            'round': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'matches'", 'to': u"orm['tournament.Round']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'matches'", 'symmetrical': 'False', 'through': u"orm['tournament.MatchResult']", 'to': u"orm['tournament.Team']"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'won_matches'", 'null': 'True', 'to': u"orm['tournament.Team']"})
        },
        u'tournament.matchreport': {
            'Meta': {'object_name': 'MatchReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reports'", 'to': u"orm['tournament.Match']"}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'opponents_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Player']", 'symmetrical': 'False'}),
            'submitted_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Team']"}),
            'team_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'tournament.matchresult': {
            'Meta': {'object_name': 'MatchResult'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_win': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Match']"}),
            'opponents_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'points_diff': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'score_diff': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'results'", 'to': u"orm['tournament.Team']"})
        },
        u'tournament.player': {
            'Meta': {'object_name': 'Player'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'steamid': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'tournament.round': {
            'Meta': {'object_name': 'Round'},
            'ends_at': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_scheduled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'round_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'starts_at': ('django.db.models.fields.DateField', [], {}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Tournament']"})
        },
        u'tournament.team': {
            'Meta': {'object_name': 'Team'},
            'has_server': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leader': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'team'", 'to': u"orm['steamauth.SteamUser']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'tournament.teamtournament': {
            'Meta': {'object_name': 'TeamTournament'},
            'forfeits': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_alive': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'ladder_points': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'ladder_points_lost': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'ladder_points_won': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points_lost': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points_won': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'matches_played': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'opponents': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Team']", 'symmetrical': 'False'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stats'", 'to': u"orm['tournament.Team']"}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Tournament']"})
        },
        u'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'config': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Config']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ladder_rounds': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'playoff_rounds': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'}),
            'starts_at': ('django.db.models.fields.DateField', [], {}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'tournaments'", 'symmetrical': 'False', 'through': u"orm['tournament.TeamTournament']", 'to': u"orm['tournament.Team']"})
        }
    }

    complete_apps = ['tournament']