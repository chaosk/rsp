# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Map'
        db.create_table(u'tournament_map', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=70)),
            ('map_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('config', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tournament.Config'])),
        ))
        db.send_create_signal(u'tournament', ['Map'])

        # Deleting field 'Tournament.config'
        db.delete_column(u'tournament_tournament', 'config_id')

        # Adding M2M table for field maps on 'Tournament'
        db.create_table(u'tournament_tournament_maps', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tournament', models.ForeignKey(orm[u'tournament.tournament'], null=False)),
            ('map', models.ForeignKey(orm[u'tournament.map'], null=False))
        ))
        db.create_unique(u'tournament_tournament_maps', ['tournament_id', 'map_id'])


    def backwards(self, orm):
        # Deleting model 'Map'
        db.delete_table(u'tournament_map')


        # User chose to not deal with backwards NULL issues for 'Tournament.config'
        raise RuntimeError("Cannot reverse this migration. 'Tournament.config' and its values cannot be restored.")
        # Removing M2M table for field maps on 'Tournament'
        db.delete_table('tournament_tournament_maps')


    models = {
        u'steamauth.steamuser': {
            'Meta': {'ordering': "['created_at']", 'object_name': 'SteamUser'},
            'avatar': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'profile_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'steamid': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'tournament.config': {
            'Meta': {'object_name': 'Config'},
            'config_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'})
        },
        u'tournament.map': {
            'Meta': {'object_name': 'Map'},
            'config': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Config']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'map_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'})
        },
        u'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'home_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'home_matches'", 'to': u"orm['tournament.Team']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'matches'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['tournament.Player']"}),
            'round': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'matches'", 'to': u"orm['tournament.Round']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'matches'", 'symmetrical': 'False', 'through': u"orm['tournament.MatchResult']", 'to': u"orm['tournament.Team']"}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'won_matches'", 'null': 'True', 'to': u"orm['tournament.Team']"})
        },
        u'tournament.matchreport': {
            'Meta': {'object_name': 'MatchReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reports'", 'to': u"orm['tournament.Match']"}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'opponents_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Player']", 'symmetrical': 'False'}),
            'submitted_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Team']"}),
            'team_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'tournament.matchresult': {
            'Meta': {'object_name': 'MatchResult'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_win': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Match']"}),
            'opponents_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'points_diff': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'score_diff': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'results'", 'to': u"orm['tournament.Team']"})
        },
        u'tournament.player': {
            'Meta': {'object_name': 'Player'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'steamid': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'tournament.round': {
            'Meta': {'object_name': 'Round'},
            'ends_at': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_scheduled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'round_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'starts_at': ('django.db.models.fields.DateField', [], {}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Tournament']"})
        },
        u'tournament.team': {
            'Meta': {'object_name': 'Team'},
            'has_server': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leader': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'teams'", 'to': u"orm['steamauth.SteamUser']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'tournament.teamtournament': {
            'Meta': {'object_name': 'TeamTournament'},
            'forfeits': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_alive': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'ladder_points': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'ladder_points_lost': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'ladder_points_won': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points_lost': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points_won': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'matches_played': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'opponents': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Team']", 'symmetrical': 'False'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stats'", 'to': u"orm['tournament.Team']"}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Tournament']"})
        },
        u'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ladder_rounds': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'maps': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Map']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'playoff_rounds': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'}),
            'starts_at': ('django.db.models.fields.DateField', [], {}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'tournaments'", 'symmetrical': 'False', 'through': u"orm['tournament.TeamTournament']", 'to': u"orm['tournament.Team']"})
        }
    }

    complete_apps = ['tournament']