# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'TeamTournament.is_ready'
        db.add_column(u'tournament_teamtournament', 'is_ready',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding unique constraint on 'Player', fields ['steamid']
        db.create_unique(u'tournament_player', ['steamid'])


    def backwards(self, orm):
        # Removing unique constraint on 'Player', fields ['steamid']
        db.delete_unique(u'tournament_player', ['steamid'])

        # Deleting field 'TeamTournament.is_ready'
        db.delete_column(u'tournament_teamtournament', 'is_ready')


    models = {
        u'steamauth.steamuser': {
            'Meta': {'ordering': "['created_at']", 'object_name': 'SteamUser'},
            'avatar': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'level': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'profile_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'steamid': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'tournament.config': {
            'Meta': {'object_name': 'Config'},
            'config_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'extension': ('django.db.models.fields.CharField', [], {'default': "'cfg'", 'max_length': '5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'})
        },
        u'tournament.map': {
            'Meta': {'object_name': 'Map'},
            'config': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Config']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'})
        },
        u'tournament.match': {
            'Meta': {'object_name': 'Match'},
            'home_tt': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'home_matches'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['tournament.TeamTournament']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'matches'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['tournament.Player']"}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.MatchReport']", 'null': 'True', 'blank': 'True'}),
            'round': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'matches'", 'to': u"orm['tournament.Round']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'}),
            'tts': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'matches'", 'symmetrical': 'False', 'through': u"orm['tournament.MatchResult']", 'to': u"orm['tournament.TeamTournament']"}),
            'winner_tt': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'won_matches'", 'null': 'True', 'to': u"orm['tournament.TeamTournament']"})
        },
        u'tournament.matchreport': {
            'Meta': {'object_name': 'MatchReport'},
            'contestation_notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'default_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'opponents_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Player']", 'symmetrical': 'False'}),
            'submitted_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Team']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'team_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'tournament.matchresult': {
            'Meta': {'ordering': "['-id']", 'object_name': 'MatchResult'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_defaultloss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_defaultwin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_win': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'results'", 'to': u"orm['tournament.Match']"}),
            'opponent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Team']", 'null': 'True', 'blank': 'True'}),
            'opponents_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'points_diff': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'score_diff': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'tt': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'results'", 'to': u"orm['tournament.TeamTournament']"})
        },
        u'tournament.player': {
            'Meta': {'object_name': 'Player'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'steamid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'})
        },
        u'tournament.round': {
            'Meta': {'object_name': 'Round'},
            'ends_at': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_scheduled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'played_map': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Map']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'round_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'starts_at': ('django.db.models.fields.DateField', [], {}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rounds'", 'to': u"orm['tournament.Tournament']"})
        },
        u'tournament.rules': {
            'Meta': {'object_name': 'Rules'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'rules_html': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'rules_raw': ('django.db.models.fields.TextField', [], {}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'})
        },
        u'tournament.team': {
            'Meta': {'object_name': 'Team'},
            'has_server': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leader': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'teams'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['steamauth.SteamUser']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'tournament.teamtournament': {
            'Meta': {'object_name': 'TeamTournament'},
            'forfeits': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_alive': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'is_ready': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_suspended': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ladder_points': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'ladder_points_lost': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'ladder_points_won': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points_lost': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'match_points_won': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'matches_played': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'opponents': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['tournament.Team']", 'symmetrical': 'False'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stats'", 'to': u"orm['tournament.Team']"}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tts'", 'to': u"orm['tournament.Tournament']"})
        },
        u'tournament.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'configs': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'tournaments'", 'symmetrical': 'False', 'to': u"orm['tournament.Config']"}),
            'ends_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ladder_rounds': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'playoff_rounds': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'rules': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['tournament.Rules']"}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '70'}),
            'staff': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'tournaments'", 'symmetrical': 'False', 'to': u"orm['steamauth.SteamUser']"}),
            'starts_at': ('django.db.models.fields.DateField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'O'", 'max_length': '1'}),
            'team_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'teams': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'tournaments'", 'symmetrical': 'False', 'through': u"orm['tournament.TeamTournament']", 'to': u"orm['tournament.Team']"})
        }
    }

    complete_apps = ['tournament']