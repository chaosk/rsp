import os 
import datetime
from datetime import date, timedelta
from django.conf import settings
from django.core import mail
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Sum
from django.template.loader import render_to_string
import persistent_messages
import markdown
from steamauth.utils import STEAMID_MAGIC
from tournament.utils import embedly

"""
Now, some theoretical calls:

Ranks denormalized:
tournament = Tournament.objects.get(slug='rsp02eu')
Team.objects.filter(tournaments=tournament).select_related(
	).order_by('-stats__ladder_points', '-stats__match_points')
"""

STEAMCOMMUNITY_PROFILE_URL = "http://steamcommunity.com/profiles/{0}"

MESSAGE_YOUR_OPPONENT = "is <a href=\"{0}\">{1}</a>! " \
	"Contact their leader, " \
	"<a href=\"{2}\">{3}</a>, right away " \
	"to schedule your match."


class TournamentException(Exception):
	pass


class Tournament(models.Model):
	name = models.CharField(max_length=70)
	short_name = models.CharField(max_length=50, blank=True)
	slug = models.CharField(max_length=70, unique=True)
	starts_at = models.DateField()
	ends_at = models.DateField(null=True, blank=True)

	rules = models.ForeignKey('Rules')
	configs = models.ManyToManyField('Config', related_name='tournaments')

	teams = models.ManyToManyField('Team', through='TeamTournament',
		related_name='tournaments')

	is_featured = models.BooleanField(default=False)

	staff = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='tournaments')

	"""
	Teams are free to sign up
	"""
	STATE_OPEN = 'O'

	"""
	Teams cannot sign up, but staff can modify tournament properties
	(including adding new teams)
	"""
	STATE_CLOSED = 'C'

	"""
	All properties are frozen, tournament has been generated.
	"""
	STATE_PENDING = 'P'

	"""
	First round has started.
	"""
	STATE_IN_PROGRESS = 'I'

	"""
	Last round has ended.
	"""
	STATE_FINISHED = 'F'

	STATE_CHOICES = (
		(STATE_OPEN, "Open"),
		(STATE_CLOSED, "Closed"),
		(STATE_PENDING, "Pending"),
		(STATE_IN_PROGRESS, "In progress"),
		(STATE_FINISHED, "Finished")
	)
	state = models.CharField(max_length=1, choices=STATE_CHOICES,
		default=STATE_OPEN)

	objects = models.Manager()

	"""
	ladder_rounds specifies how many ladder rounds will be played.

	Setting this to zero enforces playoff_rounds = ceil(log2(number_of_teams))
	(meaning tournament becomes single elimination).

	2 field cannot be changed after tournament has been created.
	"""
	ladder_rounds = models.PositiveSmallIntegerField()

	"""
	playoff_rounds set to:
	  - 0 will disable play-offs.
	  - 1 will cause best two teams play in a final.
	  - 2 will cause best four teams play in semifinals and in a final.
	  and so on

	This field cannot be changed after tournament has been created.
	"""
	playoff_rounds = models.PositiveSmallIntegerField()

	# denormalized

	team_count = models.PositiveSmallIntegerField(default=0)

	@property
	def current_round(self):
		try:
			round = self.rounds.filter(ends_at__gt=date.today()).order_by('number')[0]
		except IndexError:
			round = None
		return round

	@property
	def previous_round(self):
		return self.rounds.filter(ends_at__lte=date.today()).order_by('-number')[0]

	@property
	def active_tts_count(self):
		return self.tts.filter(is_suspended=False).count()

	def has_started(self):
		return self.starts_at <= date.today()

	def has_ended(self):
		return self.ends_at <= date.today()

	def is_happening(self):
		# return self.has_started() and not self.has_ended()
		return self.starts_at <= date.today() < self.ends_at

	def get_absolute_url(self):
		return reverse('tournament_details', args=[self.slug])

	def __unicode__(self):
		return self.name

	def save(self, *args, **kwargs):
		"""
		@TODO

		Creates n=ladder_rounds ladder rounds.
		Creates n=playoff_rounds playoff rounds.
		"""
		created = not self.id
		if created:
			self.ends_at = self.starts_at \
				+ timedelta(days=7*(self.ladder_rounds+self.playoff_rounds))
		super(Tournament, self).save(*args, **kwargs)
		if created:
			next_round_starts_at = self.starts_at
			for number in range(1, self.ladder_rounds+self.playoff_rounds+1):
				around = Round()
				around.number = number
				around.tournament = self
				around.starts_at = next_round_starts_at
				around.ends_at = next_round_starts_at + timedelta(days=6)
				next_round_starts_at += timedelta(days=7)
				around.round_type = Round.ROUND_TYPE_LADDER \
					if number <= self.ladder_rounds else Round.ROUND_TYPE_PLAYOFF
				around.save()


def map_file_name(instance, filename):
	"""
	Determines map path and file name.
	"""
	return 'maps/{0}.bsp'.format(instance.slug)


class Map(models.Model):
	slug = models.CharField(unique=True, max_length=70,
			help_text="Map name (also its filename)")
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	map_file = models.FileField(upload_to=map_file_name)
	config = models.ForeignKey('Config', null=True, blank=True,
		on_delete=models.SET_NULL)

	def get_absolute_url(self):
		return reverse('map_details', args=[self.slug])

	def __unicode__(self):
		return self.slug


def config_file_name(instance, filename):
	"""
	Determines config path and file name.
	"""
	return 'configs/{0}.{1}'.format(instance.slug,
		instance.extension)


class Config(models.Model):
	name = models.CharField(max_length=70, help_text="Config name")
	slug = models.CharField(unique=True, max_length=70,
		help_text="Config short name (also its filename)")
	extension = models.CharField(max_length=5,
		default='cfg')
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	description = models.CharField(max_length=255, blank=True)
	config_file = models.FileField(upload_to=config_file_name)

	def get_absolute_url(self):
		return reverse('config_details', args=[self.slug])

	def __unicode__(self):
		return self.name


class Round(models.Model):
	"""
	Number of rounds is based on number
	of entrants and number of groups.

	@TODO ask about more details

	Total number of rounds is
	  group stage rounds + play-off rounds
	"""
	number = models.PositiveSmallIntegerField(null=True)
	tournament = models.ForeignKey(Tournament, related_name='rounds')

	played_map = models.ForeignKey('Map', null=True, blank=True,
		on_delete=models.SET_NULL)

	starts_at = models.DateField()
	ends_at = models.DateField()

	"""
	Defines whether match pairs have been generated.
	"""
	is_scheduled = models.BooleanField(default=False)

	is_postprocessed = models.BooleanField(default=False)

	ROUND_TYPE_GROUP = 'G'
	ROUND_TYPE_LADDER = 'L'
	ROUND_TYPE_PLAYOFF = 'P'
	ROUND_TYPE_CHOICES = (
		(ROUND_TYPE_GROUP, 'Group stage'),
		(ROUND_TYPE_LADDER, 'Ladder'),
		(ROUND_TYPE_PLAYOFF, 'Play-off'),
	)
	round_type = models.CharField(max_length=1, choices=ROUND_TYPE_CHOICES)

	def has_started(self):
		return self.starts_at <= date.today()

	def has_ended(self):
		return self.ends_at <= date.today()

	def is_happening(self):
		# return self.has_started() and not self.has_ended()
		return self.starts_at <= date.today() < self.ends_at

	def __unicode__(self):
		return u"Round {0}".format(self.number)

	def generate_pairs(self):
		if self.is_scheduled:
			raise TournamentException(
				"Match pairs for this round has already been generated."
			)

		connection = mail.get_connection()
		connection.open()

		if self.round_type == Round.ROUND_TYPE_PLAYOFF:
			try:
				previous_round = self.tournament.rounds.get(number=self.number-1)
			except Round.DoesNotExist:
				# first round at all, single elimination tournament
				self.tournament.tts.update(is_alive=True)
			else:
				if previous_round.round_type != Round.ROUND_TYPE_PLAYOFF:
					if self.tournament.playoff_rounds == 1:
						promoted_teams = 2
					else:
						promoted_teams = self.tournament.playoff_rounds ** 2
					promoted_tts = self.tournament.tts.exclude(is_suspended=True).order_by(
						'-ladder_points', '-match_points', 'forfeits',
						'-match_points_won', 'matches_played', '-tiebreaker'
					)[:promoted_teams]
					TeamTournament.objects.filter(pk__in=promoted_tts).update(is_alive=True)

			# now, generate things
			teams = list(self.tournament.tts.filter(is_alive=True).order_by(
						'rank',
						'-ladder_points', '-ladder_points_won', '-match_points', 
						'forfeits', '-match_points_won', 'matches_played', '-tiebreaker'
					))
			while True:
				if not len(teams):
					# no more pairs!
					break
				first_team = teams[0] # higher seeded team
				del teams[0] # remove it from team list
				if not len(teams):
					# no team to pair with the last team!
					# a bye?
					match = Match()
					match.round = self
					match.home_tt = match.winner_tt = first_team
					match.status = Match.STATUS_BYE
					match.save()
					result = MatchResult(match=match, tt=match.winner_tt,
						score=1, opponents_score=0, points_diff=1, 
						is_win=True)
					result.save()

					first_team.ladder_points_won += 1
					first_team.ladder_points += 1
					first_team.matches_played += 1
					first_team.match_points += 1
					first_team.match_points_won += 1
					first_team.save()

					persistent_messages.add_async_message(
						persistent_messages.SUCCESS,
						"does not exist! You've got a bye, which means "
						" you (sadly) don't get to play this week.",
						subject="Your next round opponent",
						user=first_team.team.leader,
					)
					if first_team.team.leader.email and first_team.team.leader.wants_emails:
						email = mail.EmailMessage(
							u"No opponent for you this time",
							render_to_string('tournament/email/new_round_bye.txt', {
								'user': first_team.team.leader,
							}),
							to=[first_team.team.leader.email], connection=connection)
						email.send()
					#match.teams.add(first_team['id'])
					break
				
				# always match first with the last.
				second_team = teams[-1]
				del teams[-1]

				match = Match()
				match.round = self
				if first_team.team.has_server:
					match.home_tt = first_team
				else:
					match.home_tt = second_team
				match.save()
				result = MatchResult()
				result.match = match
				result.tt = first_team
				result.opponent = second_team.team
				result.save()
				result = MatchResult()
				result.match = match
				result.tt = second_team
				result.opponent = first_team.team
				result.save()
				persistent_messages.add_async_message(
					persistent_messages.SUCCESS,
					MESSAGE_YOUR_OPPONENT.format(
						second_team.team.get_absolute_url(),
						second_team.team,
						second_team.team.leader.get_absolute_url(),
						second_team.team.leader,
					),
					subject="Your next round opponent",
					user=first_team.team.leader,
				)

				if first_team.team.leader.email and first_team.team.leader.wants_emails:
					email = mail.EmailMessage(
						u"Your next round opponent",
						render_to_string('tournament/email/new_round.txt', {
							'user': first_team.team.leader,
							'opponent_team': second_team.team,
							'round': self,
						}),
						to=[first_team.team.leader.email], connection=connection)
					email.send()
				persistent_messages.add_async_message(
					persistent_messages.SUCCESS,
					MESSAGE_YOUR_OPPONENT.format(
						first_team.team.get_absolute_url(),
						first_team.team,
						first_team.team.leader.get_absolute_url(),
						first_team.team.leader,
					),
					subject="Your next round opponent",
					user=second_team.team.leader,
				)

				if second_team.team.leader.email and second_team.team.leader.wants_emails:
					email = mail.EmailMessage(
						u"Your next round opponent",
						render_to_string('tournament/email/new_round.txt', {
							'user': second_team.team.leader,
							'opponent_team': first_team.team,
							'round': self,
						}),
						to=[second_team.team.leader.email], connection=connection)
					email.send()
			self.is_scheduled = True
			self.save()
		elif self.round_type == Round.ROUND_TYPE_LADDER:
			teams = list(TeamTournament.objects.filter(
					tournament=self.tournament
				).exclude(is_suspended=True).order_by(
					'-ladder_points', '-ladder_points_won', '-match_points',
					'forfeits', '-match_points_won', '-tiebreaker'))
			while True:
				if not len(teams):
					# no more pairs!
					break
				first_team = teams[0] # first unmatched team
				del teams[0] # remove it from team list
				if not len(teams):
					# no team to pair with the last team!
					# a bye, i guess.
					match = Match()
					match.round = self
					match.home_tt = match.winner_tt = first_team
					match.status = Match.STATUS_BYE
					match.save()

					first_team.ladder_points_won += 1
					first_team.ladder_points += 1
					first_team.matches_played += 1
					first_team.match_points += 1
					first_team.match_points_won += 1
					first_team.save()

					result = MatchResult(match=match, tt=match.winner_tt,
						score=1, opponents_score=0, points_diff=1,
						is_win=True)
					result.save()
					persistent_messages.add_async_message(
						persistent_messages.SUCCESS,
						"does not exist! You've got a bye, which means "
						" you (sadly) don't get to play this week.",
						subject="Your next round opponent",
						user=first_team.team.leader,
					)
					if first_team.team.leader.email and first_team.team.leader.wants_emails:
						email = mail.EmailMessage(
							u"No opponent for you this time",
							render_to_string('tournament/email/new_round_bye.txt', {
								'user': first_team.team.leader,
							}),
							to=[first_team.team.leader.email], connection=connection)
						email.send()
					#match.teams.add(first_team['id'])
					break

				opponents = first_team.opponents.all()
				for i in range(len(teams)):
					if teams[i].team in opponents:
						# first_team already played against this team
						continue
					# second team found, prepare a fixture
					match = Match()
					match.round = self
					if first_team.team.has_server:
						match.home_tt = first_team
					else:
						match.home_tt = teams[i]
					match.save()
					result = MatchResult()
					result.match = match
					result.tt = first_team
					result.opponent = teams[i].team
					result.save()
					result = MatchResult()
					result.match = match
					result.tt = teams[i]
					result.opponent = first_team.team
					result.save()
					first_team.opponents.add(teams[i].team)
					teams[i].opponents.add(first_team.team)
					if first_team.team.leader.email and first_team.team.leader.wants_emails:
						email = mail.EmailMessage(
							u"Your next round opponent",
							render_to_string('tournament/email/new_round.txt', {
								'user': first_team.team.leader,
								'opponent_team': teams[i].team,
								'round': self,
							}),
							to=[first_team.team.leader.email], connection=connection)
						email.send()
					persistent_messages.add_async_message(
						persistent_messages.SUCCESS,
						MESSAGE_YOUR_OPPONENT.format(
							teams[i].team.get_absolute_url(),
							teams[i].team,
							teams[i].team.leader.get_absolute_url(),
							teams[i].team.leader,
						),
						subject="Your next round opponent",
						user=first_team.team.leader,
					)
					if teams[i].team.leader.email and teams[i].team.leader.wants_emails:
						email = mail.EmailMessage(
							u"Your next round opponent",
							render_to_string('tournament/email/new_round.txt', {
								'user': teams[i].team.leader,
								'opponent_team': first_team.team,
								'round': self,
							}),
							to=[teams[i].team.leader.email], connection=connection)
						email.send()
					persistent_messages.add_async_message(
						persistent_messages.SUCCESS,
						MESSAGE_YOUR_OPPONENT.format(
							first_team.team.get_absolute_url(),
							first_team.team,
							first_team.team.leader.get_absolute_url(),
							first_team.team.leader,
						),
						subject="Your next round opponent",
						user=teams[i].team.leader,
					)
					del teams[i] # remove matched team
					break
			self.is_scheduled = True
			self.save()
		connection.close()

	def report_accepted_count(self):
		return self.matches.filter(status__in=(Match.STATUS_ACCEPTED,
			Match.STATUS_RESOLVED, Match.STATUS_BYE, Match.STATUS_DEFAULTLOSS)).count()

	def report_reported_count(self):
		return self.matches.filter(status__in=(Match.STATUS_REPORTED,
			Match.STATUS_CONTESTED)).count()

	def report_contested_count(self):
		return self.matches.filter(status=Match.STATUS_CONTESTED).count()

	def save(self, *args, **kwargs):
		created = not self.id
		if created:
			try:
				last_round = Round.objects.filter(
					tournament=self.tournament).order_by('-number')[0]
				number = last_round.number + 1
			except IndexError:
				number = 1
			self.number = number
		super(Round, self).save(*args, **kwargs)
			

class Match(models.Model):
	round = models.ForeignKey(Round, related_name='matches')

	home_tt = models.ForeignKey('TeamTournament', related_name='home_matches',
		on_delete=models.SET_NULL, null=True, blank=True)
	tts = models.ManyToManyField('TeamTournament', through='MatchResult',
		related_name='matches')
	winner_tt = models.ForeignKey('TeamTournament', related_name='won_matches',
		null=True, blank=True)

	players = models.ManyToManyField('Player', related_name='matches',
		null=True, blank=True)

	report = models.ForeignKey('MatchReport', null=True, blank=True)

	STATUS_SCHEDULED = 'S'
	STATUS_REPORTED = 'R'
	STATUS_ACCEPTED = 'A'
	STATUS_CONTESTED = 'C'
	STATUS_RESOLVED = 'E'
	STATUS_DEFAULTLOSS = 'D'
	STATUS_BYE = 'B'
	STATUS_CHOICES = (
		(STATUS_SCHEDULED, "Scheduled"),
		(STATUS_REPORTED, "Reported"),
		(STATUS_ACCEPTED, "Accepted"),
		(STATUS_CONTESTED, "Contested"),
		(STATUS_RESOLVED, "Resolved"),
		(STATUS_DEFAULTLOSS, "Double forfeit"),
		(STATUS_BYE, "Bye"),
	)
	status = models.CharField(max_length=1, choices=STATUS_CHOICES,
		default=STATUS_SCHEDULED)

	def post_accept(self):
		self.status = Match.STATUS_ACCEPTED
		first_tt = self.tts.get(team=self.report.submitted_by,
			tournament=self.round.tournament)
		second_tt = self.tts.exclude(team=self.report.submitted_by).get(
			tournament=self.round.tournament)
		first_result = self.results.get(tt=first_tt)
		second_result = self.results.get(tt=second_tt)
		if self.report.default_win:
			winner_tt = first_tt
			loser_tt = second_tt
			first_result.score = 1
			first_result.opponents_score = 0
			first_result.is_defaultwin = True
			second_result.score = 0
			second_result.opponents_score = 4
			second_result.is_defaultloss = True
		else:
			self.players.add(*list(self.report.players.all()))
			if self.report.team_score > self.report.opponents_score:
				winner_tt = first_tt
				loser_tt = second_tt
			else:
				winner_tt = second_tt
				loser_tt = first_tt
			first_result.score = self.report.team_score
			first_result.opponents_score = self.report.opponents_score
			second_result.score = self.report.opponents_score
			second_result.opponents_score = self.report.team_score

		self.winner_tt = winner_tt
		if self.round.round_type == Round.ROUND_TYPE_PLAYOFF:
			loser_tt.is_alive = False
			loser_tt.save()

		self.save()

		first_result.save()
		second_result.save()

		first_tt.denormalize()
		second_tt.denormalize()

	def is_accepted(self):
		return self.status in (Match.STATUS_ACCEPTED, Match.STATUS_RESOLVED)

	def confirmation_ends_at(self):
		if not self.report:
			return None
		deadline = self.report.submitted_at + timedelta(days=1)
		ends_at = datetime.datetime.combine(self.round.ends_at, datetime.time.min)
		return ends_at if deadline < ends_at else deadline

	def can_report(self):
		return self.round.ends_at > datetime.date.today()

	def can_confirm(self):
		if not self.report or self.status != Match.STATUS_REPORTED:
			return False
		return self.confirmation_ends_at() >= datetime.datetime.now()

	def get_absolute_url(self):
		return reverse('match_details', kwargs={'match_id': self.id})


class MatchChange(models.Model):
	match = models.ForeignKey(Match, related_name='admin_changes')
	created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
		null=True, blank=True, on_delete=models.SET_NULL)
	created_at = models.DateTimeField(auto_now_add=True)
	action = models.CharField(max_length=255)
	notes = models.TextField(blank=True)


class MatchReport(models.Model):
	submitted_at = models.DateTimeField(auto_now_add=True)
	submitted_by = models.ForeignKey('Team', null=True, blank=True,
		on_delete=models.SET_NULL)
	team_score = models.PositiveSmallIntegerField()
	opponents_score = models.PositiveSmallIntegerField('Opponent score')

	notes = models.TextField(blank=True)
	contestation_notes = models.TextField(blank=True,
		help_text="Describe what's wrong with the report. A staff member will contact both team leaders soon.")

	default_win = models.BooleanField('Default win', default=False,
		help_text='Did your opponent forfeit the match?')

	players = models.ManyToManyField('Player')


class MatchResult(models.Model):
	"""
	Intermediary model for M2M relation between Match and TeamTournament.
	"""
	match = models.ForeignKey(Match, related_name='results')
	tt = models.ForeignKey('TeamTournament', related_name='results')
	opponent = models.ForeignKey('Team', null=True, blank=True)
	score = models.PositiveSmallIntegerField(null=True)
	opponents_score = models.PositiveSmallIntegerField(null=True)

	is_defaultloss = models.BooleanField(default=False)
	is_defaultwin = models.BooleanField(default=False)

	# denormalized

	# these are used in ladder, leave empty in playoffs
	"""
	points_diff - Ladder points difference
	 -1 if loss
	  1 if win
	"""
	points_diff = models.SmallIntegerField(null=True)

	"""
	score_diff - Match points difference
	"""
	score_diff = models.SmallIntegerField(null=True)

	# these are used in playoffs
	is_win = models.NullBooleanField(default=None)

	class Meta:
		ordering = ['-id']

	def clear(self):
		self.score = None
		self.opponents_score = None
		self.is_defaultloss = False
		self.is_defaultwin = False
		self.points_diff = None
		self.score_diff = None

	def save(self, *args, **kwargs):
		if self.match.status != Match.STATUS_SCHEDULED:
			score_diff = self.score - self.opponents_score
			if not self.points_diff:
				self.points_diff = 1 if score_diff > 0 else -1
			if not self.score_diff:
				self.score_diff = score_diff
			if self.match.status != Match.STATUS_BYE:
				self.is_win = score_diff > 0
		super(MatchResult, self).save(*args, **kwargs)


def demo_file_name(instance, filename):
	"""
	Determines map path and file name.
	"""
	try:
		latest = MatchDemo.objects.latest('id').id
	except MatchDemo.DoesNotExist:
		latest = 0
	return 'demos/{0}/{1}/{0}-r{1}-m{2}i{3}-h{4}-{5}.dem'.format(
		instance.match.round.tournament.slug,
		instance.match.round.number,
		instance.match.id,
		latest+1,
		instance.match.home_tt_id,
		instance.match.round.played_map.slug)


class MatchDemo(models.Model):
	demo_file = models.FileField(upload_to=demo_file_name,
		help_text="This <strong>has</strong> to be TF2 STV demo")
	created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
		null=True, blank=True, related_name='demos', on_delete=models.SET_NULL)
	created_at = models.DateTimeField(auto_now_add=True)
	match = models.ForeignKey(Match, related_name='demos')
	start_tick = models.IntegerField("Starting tick",
		help_text="This really helps people who want to watch your demo")
	notes = models.TextField(blank=True)

	def filename(self):
		return os.path.basename(self.demo_file.name)

class MatchCast(models.Model):
	title = models.CharField(max_length=255, blank=True,
		help_text="Anything <strong>besides</strong> team names, as they are displayed automatically.")
	created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
		null=True, blank=True, related_name='casts', on_delete=models.SET_NULL)
	created_at = models.DateTimeField(auto_now_add=True)
	match = models.ForeignKey(Match, related_name='casts')
	url = models.URLField("URL", max_length=255, unique=True,
		help_text="Could be a YouTube link, or a TwitchTV link, or virtually anything.")

	def get_embed(self):
		return "embed:{0}".format(self.url)

	def get_embedly(self, maxwidth=560):
		return embedly(self.url, maxwidth)

	def get_thumbnail(self, maxwidth=400):
		return embedly(self.url, maxwidth, thumbnail=True)

	def get_absolute_url(self):
		return reverse('match_cast_details', args=[self.id])

	def __unicode__(self):
		return "{0} by {1}".format(self.title, self.created_by)


class Team(models.Model):
	name = models.CharField(max_length=70)
	tag = models.CharField(max_length=25)
	leader = models.ForeignKey(settings.AUTH_USER_MODEL,
		null=True, blank=True,
		related_name='teams', on_delete=models.SET_NULL)

	"""
	This field decides whether a team can be marked as a home team
	"""
	has_server = models.BooleanField()

	def get_absolute_url(self):
		return reverse('team_details', kwargs={'team_id': self.id})

	def get_tt(self):
		return self.stats.latest('id')

	def __unicode__(self):
		return self.name


class TeamTournament(models.Model):
	"""
	Intermediary model for M2M relation between Tournament and Team.

	Also, I'm really bad at naming models.
	"""

	tournament = models.ForeignKey(Tournament, related_name='tts')
	team = models.ForeignKey(Team, related_name='stats')

	opponents = models.ManyToManyField(Team)

	is_ready = models.BooleanField(default=False)
	is_suspended = models.BooleanField(default=False)
	suspended_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
	suspended_at = models.DateTimeField(blank=True, null=True)
	suspended_for = models.CharField(max_length=255, blank=True)

	rank = models.SmallIntegerField(default=99)

	tiebreaker = models.SmallIntegerField(default=0)
	# denormalized

	# these are used in ladder
	forfeits = models.SmallIntegerField(default=0)

	matches_played = models.SmallIntegerField(default=0)

	ladder_points = models.SmallIntegerField(default=0)
	ladder_points_won = models.SmallIntegerField(default=0)
	ladder_points_lost = models.SmallIntegerField(default=0)

	match_points = models.SmallIntegerField(default=0)
	match_points_won = models.SmallIntegerField(default=0)
	match_points_lost = models.SmallIntegerField(default=0)

	# these are used in playoffs
	is_alive = models.NullBooleanField(default=None)

	def denormalize(self):
		played = self.results.filter(
			match__status__in=(Match.STATUS_ACCEPTED, Match.STATUS_RESOLVED,
				Match.STATUS_BYE, Match.STATUS_DEFAULTLOSS))
		self.forfeits = self.results.filter(is_defaultloss=True).count()
		self.matches_played = played.filter(is_defaultloss=False).count()
		self.ladder_points_won = played.filter(is_win=True).count()
		self.ladder_points_lost = played.filter(is_win=False).count()
		self.ladder_points = self.ladder_points_won - self.ladder_points_lost
		aggregated_scores = played.aggregate(score_sum=Sum('score'),
			opponents_score_sum=Sum('opponents_score'))
		self.match_points_won = aggregated_scores['score_sum'] or 0
		self.match_points_lost = aggregated_scores['opponents_score_sum'] or 0
		self.match_points = self.match_points_won - self.match_points_lost
		self.save()

	def __unicode__(self):
		return u"{0} in {1}".format(self.team, self.tournament)


class Rules(models.Model):
	name = models.CharField(max_length=70)
	slug = models.CharField(max_length=70, unique=True)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	rules_raw = models.TextField(help_text="Use Markdown syntax")
	rules_html = models.TextField(blank=True, null=True)

	def __unicode__(self):
		return self.name

	def get_absolute_url(self):
		return reverse('rules_details', args=[self.slug])

	def save(self, *args, **kwargs):
		self.rules_html = markdown.markdown(self.rules_raw)
		super(Rules, self).save(*args, **kwargs)


class Player(models.Model):
	steamid = models.CharField(max_length=20, unique=True)
	nickname = models.CharField(max_length=100)

	played_for = models.ForeignKey(Team,
		blank=True, null=True)
	matches_played = models.SmallIntegerField(default=0)

	MEDAL_OVERRIDES = (
		(True, "Grant"),
		(None, "Neutral"),
		(False, "Deny"),
	)
	force_medals = models.NullBooleanField(default=None, choices=MEDAL_OVERRIDES)
	medal_given = models.BooleanField(default=False)
	medal_changed_by = models.ForeignKey(settings.AUTH_USER_MODEL,
		blank=True, null=True)
	medal_changed_at = models.DateTimeField(blank=True, null=True)

	@property
	def steamid64(self):
		authn, steamid = self.steamid.split(":")[1:]
		return int(authn) + STEAMID_MAGIC + int(steamid) * 2

	@property
	def profile_url(self):
		return STEAMCOMMUNITY_PROFILE_URL.format(self.steamid64)

	def __unicode__(self):
		return self.nickname or unicode(self.steamid)

	def get_absolute_url(self):
		return reverse('player_details', args=[self.steamid])
