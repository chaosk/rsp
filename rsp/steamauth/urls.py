from django.conf.urls import patterns, url

urlpatterns = patterns('steamauth.views',
	url(r'^signin/$', 'login_begin', name='signin'),
	url(r'^hello/$', 'login_complete', name='signed-in'),
	url(r'^bye/$', 'logout', name='signout'),
	url(r'^mailing/subscribe/$', 'subscribe', name='subscribe'),
	url(r'^mailing/unsubscribe/$', 'unsubscribe', name='unsubscribe'),
	url(r'^users/$', 'user_list', name='user_list'),
	url(r'^u/(?P<steamid>\d+)/$', 'user_details', name='user_details'),
	url(r'^u/(?P<steamid>\d+)/edit/$', 'user_edit', name='user_edit'),
	url(r'^u/(?P<steamid>\d+)/ban/$', 'user_ban', name='user_ban'),
	url(r'^u\+/$', 'user_add', name='user_add'),
)
