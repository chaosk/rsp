from django.forms import ModelForm
from django.contrib.auth import get_user_model
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class UserAddForm(ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_action = 'user_add'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(UserAddForm, self).__init__(*args, **kwargs)

	class Meta:
		model = get_user_model()
		fields = ('steamid', 'email', 'wants_emails', 'level')


class UserEditForm(ModelForm):

	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_method = 'post'
		self.helper.form_class = 'form-horizontal'
		self.helper.add_input(Submit('submit', 'Submit'))
		super(UserEditForm, self).__init__(*args, **kwargs)

	class Meta:
		model = get_user_model()
		fields = ('steamid', 'email', 'wants_emails',
			'is_active', 'freeze_username', 'level')
