from django.conf import settings
from django.conf.urls import patterns, include, url

import autocomplete_light
autocomplete_light.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from django.views.generic import TemplateView


urlpatterns = patterns('',
	# Examples:
	url(r'', include('tournament.urls')),
	url(r'', include('steamauth.urls')),
	url(r'autocomplete/', include('autocomplete_light.urls')),
	url(r'^messages/', include('persistent_messages.urls')),
	# url(r'^rsp/', include('rsp.foo.urls')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	# url(r'^admin/', include(admin.site.urls)),
)

if settings.SERVE_STATIC_FILES:
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve',
			{'document_root': settings.MEDIA_ROOT}),
	)

if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^404/$', TemplateView.as_view(template_name='404.html')),
		(r'^500/$', TemplateView.as_view(template_name='500.html'))
	)